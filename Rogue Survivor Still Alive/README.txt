'Rogue Survivor' is a free, open-source zombie survival roguelike sandbox game with unique features and (limited) modding support.
'Rogue Survivor: Still Alive' is a modification of the original Rogue Survivor Alpha 9a source code from 2012, released as open source under GNU GPL v3.

The game takes place in a town famous for being the headquarters of the powerful CHAR Corporation. You wake up amidst apocalyptic chaos and you must find ways to survive. It may even be possible to flee the city...

Survive in the city
-Play as living or undead
-Survive the zombies... and the humans
-The undeads get stronger every day 
-Find food and shelter... but you're not alone
-Reincarnate when you die and continue playing as another character
-Graphic tiles, mouse support and musics

Interact with NPCs
-Fight raiding gangs 
-AI controlled survivors are trying to survive just like you
-Trade with other survivors
-Lead a band of survivors   
-Get help from the National Guard
-Commit murders... but watch out for the police

A dynamic, procedurally generated game world
-A city with different districts, buildings, sewers and subway
-Push or destroy objects, build barricades
-Day/night cycle
-Weather 
-Things happen all the time, the world is not waiting for you

Get tools, skills and friends
-Learn skills by surviving to see a new sunrise
-Scavenge items and barricading material
-Various weapons and items  
-Get followers and turn them into useful buddies

Learn, customize and mod
-In-game manual and hint system 
-Redefine the keys
-Wide range of options to configure the gameplay and the difficulty
-Resizable game window, playable on any resolution
-More than 10 original musics
-Mod the graphics, musics, actors, items and skills
-Option of rendering and audio engines

You can find your saves, screenshots, and graveyard files in \My Documents\Rogue Survivor\ (aka \Libraries\Documents\)
You can find the following help files in the game directory:
- technical help :: HELP.txt
- information about in-game stuff :: MANUAL.txt
- Rogue Survivor license :: LICENSE.txt
- for attribution credits :: CREDITS.txt
- SMFL.NET license :: SMFL.NET license.txt

If you wish to use or share this code in any way please refer to the instructions of the original author (and copyright owner), RoguedJack, below:

--------------------------------------------------------------------------------

WHAT IS THIS

This is the game source from 2012. I did not touch it since then.
There is no data files or resources, you have to download the game for that (see next section).
There is no project files or libraries. They are probably obsolete by now anyway and I don't even know if the code will compile in recent C# compilers/IDEs.
I can't provide support for compiling or answer code/gameplay questions, it's been a long time and I've moved on. I hope you'll understand!


GET THE GAME

The original blog where you can get the game is still alive, so go there:
http://roguesurvivor.blogspot.com/
You'll need this for the resources (data files, images etc...)


WHAT YOU CAN DO WITH IT

Anything but nothing commercial please. See the license file.


THANK YOU & CLOSING WORDS

That's a thank you from ME to YOU guys!
Some of you guys have some serious dedication for this game! It is both scary and awesome... scarysome!?
Sorry for failing you by not working more on the game or being more responsive!
From time to time I secretly tried to revive the game or start a new incarnation. I didn't tell you guys because all I had to show was a couple of prototypes that didn't go very far and I didn't want to repeat the RS2 fiasco...
Anyway, I don't plan on updating the game, so go ahead and have fun!!

-RoguedJack