﻿using System;

namespace djack.RogueSurvivor.Gameplay
{
    static class ZoneAttributes
    {
        public const string IS_CHAR_OFFICE = "CHAR Office";
        public const string IS_ARMY_OFFICE = "Army Office"; //@@MP (Release 6-3)
    }
}
