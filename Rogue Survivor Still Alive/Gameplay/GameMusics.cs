﻿
namespace djack.RogueSurvivor.Gameplay
{
    static class GameMusics
    {
        const string PATH = @"Resources\Music\";

        public const string ARMY = "army";
        public const string ARMY_FILE = PATH + "RS - Army";

        public const string BIKER = "biker";
        public const string BIKER_FILE = PATH + "RS - Biker";

        public const string BLACK_OPS = "blackops"; //@@MP (Release 6-1)
        public const string BLACK_OPS_FILE = PATH + "Black Ops"; //@@MP (Release 6-1)

        public const string CHAR_RESEARCHERS = "researchers"; //@@MP (Release 8-1)
        public const string CHAR_RESEARCHERS_FILE = PATH + "RS - CHAR researchers";

        public const string CHAR_UNDERGROUND_FACILITY = "char underground facility";
        public const string CHAR_UNDERGROUND_FACILITY_FILE = PATH + "RS - CUF";

        public const string FIGHT = "fight";
        public const string FIGHT_FILE = PATH + "RS - Fight";

        public const string GANGSTA = "gangsta";
        public const string GANGSTA_FILE = PATH + "RS - Gangsta";

        public const string HEYTHERE = "heythere";
        public const string HEYTHERE_FILE = PATH + "RS - Hey There";

        public const string HOSPITAL = "hospital";
        public const string HOSPITAL_FILE = PATH + "RS - Hospital";

        public const string INSANE = "insane";
        public const string INSANE_FILE = PATH + "RS - Insane";

        public const string INTERLUDE = "interlude";
        public const string INTERLUDE_FILE = PATH + "RS - Interlude - Loop";

        public const string INTRO = "intro";
        public const string INTRO_FILE = PATH + "RS - Intro";

        public const string LIMBO = "limbo";
        public const string LIMBO_FILE = PATH + "RS - Limbo";

        public const string PLAYER_DEATH = "playerdeath";
        public const string PLAYER_DEATH_FILE = PATH + "RS - Post Mortem";

        public const string POST_RESCUE = "Post-rescue"; //@@MP (Release 6-4)
        public const string POST_RESCUE_FILE = PATH + "Post-rescue"; //@@MP (Release 6-4)

        public const string SEWERS = "sewers";
        public const string SEWERS_FILE = PATH + "RS - Sewers";

        public const string SLEEP = "sleep";
        public const string SLEEP_FILE = PATH + "RS - Sleep - Loop";

        public const string SHOPPING_MALL = "Shopping Mall"; //@@MP (Release 7-3)
        public const string SHOPPING_MALL_FILE = PATH + "Shopping Mall"; //@@MP (Release 7-3)

        public const string SUBWAY = "subway";
        public const string SUBWAY_FILE = PATH + "RS - Subway";

        public const string SURFACE = "surface"; // alpha10
        public const string SURFACE_FILE = PATH + "RS - Surface"; // alpha10

        public const string SURVIVORS = "survivors"; //@@MP - was the music that is now used for CHAR researchers (Release 8-1)
        public const string SURVIVORS_FILE = PATH + "Survivors";

        public const string TEST_MUSIC = "test_music"; //@@MP (Release 7-3)
        public const string TEST_MUSIC_FILE = PATH + "test_music";
    }
}
