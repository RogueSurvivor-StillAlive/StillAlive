﻿using System;

using djack.RogueSurvivor.Data;

namespace djack.RogueSurvivor.Engine.Items
{
    [Serializable]
    class ItemLight : Item
    {
        #region Fields
        int m_Batteries;
        #endregion

        #region Properties
        public int Batteries
        {
            get { return m_Batteries; }
            set
            {
                if (value < 0) value = 0;
                m_Batteries = Math.Min( value, (this.Model as ItemLightModel).MaxBatteries);
            }
        }

        public int FovBonus
        {
            get { return (this.Model as ItemLightModel).FovBonus; }
        }

        public bool IsFullyCharged
        {
            get { return m_Batteries >= (this.Model as ItemLightModel).MaxBatteries; }
        }

        public override string ImageID
        {
            get
            {
                if (this.IsEquipped && this.Batteries > 0)
                    return base.ImageID;
                else
                    return (this.Model as ItemLightModel).OutOfBatteriesImageID;
            }
        }

        public int MaxBatteries //@@MP (Release 6-4)
        {
            get { return (this.Model as ItemLightModel).MaxBatteries; }
        }
        #endregion

        #region Init
        public ItemLight(ItemModel model)
            : base(model)
        {
            ItemLightModel itemModel = model as ItemLightModel;
            //if (!(model is ItemLightModel)) //@@MP (Release 5-7)
            if (itemModel == null)
                throw new ArgumentException("model is not a LightModel");

            //this.Batteries = (model as ItemLightModel).MaxBatteries;
            this.Batteries = itemModel.MaxBatteries;
        }
        #endregion
    }
}
