# Still Alive readme

Rogue Survivor is a free zombie survival roguelike sandbox game with unique features.
The game takes place in a town famous for being the headquarters of the powerful CHAR Corporation. You find yourself amidst the outbreak of chaos and you must find a way to survive. 

Rogue Survivor 'Staying Alive' is a fork of the vanilla Rogue Survivor Alpha 10.1 source code from 2018, released as open source under GNU GPL v3.

If you wish to use or share this code in any way please first read \Rogue Survivor Still Alive\README.txt


Discussion of this fork is taking place here: http://roguesurvivor.proboards.com/thread/377

The Config tool (RSConfig) that accompanies the game is here: https://gitlab.com/RogueSurvivor-StillAlive/RSConfig

The website for the original game is here: http://roguesurvivor.blogspot.com/

Feel free to add to the Issues board. If you would like to contribute code please contact the owner, Mark (aka MP), via the RS forum http://roguesurvivor.proboards.com (yes, you will need to create an account) or the RS Discord (link available from the forum).
